import { Component, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { EventsService } from './shared/services/events.service';
import { CalendarComponent } from 'ap-angular2-fullcalendar/src/calendar/calendar';
import { toast } from 'angular2-materialize';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  entryComponents: [CalendarComponent]
})
export class AppComponent {
	@ViewChild('calendarContainer', { read: ViewContainerRef}) calendarContainer: ViewContainerRef;

	/** Configuración inicial del calendario */
	options = {
		defaultView: 'agendaWeek',
		header: {
			left: 'prev',
			center: 'title',
			right: 'next'
		},
		editable: true,
		eventLimit: true,
		eventClick: (event, jsEvent,) => {
			this.manageEventClick(event);
		},
		events: []
	};

	especialidades: Array<Object> = [
		{ value: 'Reumatología', name: 'Reumatología' }, 
		{ value: 'Hematología', name: 'Hematología' },
		{ value: 'Medicina general', name: 'Medicina general' }, 
		{ value: 'Químico farmacéutico', name: 'Químico farmacéutico' }, 
		{ value: 'Hepatitis', name: 'Hepatitis'}
	];

	profesionales: Array<Object> = [
		{ value: 'Guillermo Calderon', name: 'Guillermo Calderon', type: 'Reumatología' }, 
		{ value: 'Maria Camila Rendon', name: 'Maria Camila Rendon', type: 'Reumatología' },
		{ value: 'German Cano Alvarez', name: 'German Cano Alvarez ', type: 'Reumatología' }, 
		{ value: 'Gloria Quintero', name: 'Gloria Quintero', type: 'Hematología' }, 
		{ value: 'Maria Fernanda Restrepo', name: 'Maria Fernanda Restrepo', type: 'Medicina general'},
		{ value: 'Tatiana Quintana', name: 'Tatiana Quintana', type: 'Medicina general'},
		{ value: 'Valentina Rendon', name: 'Valentina Rendon', type: 'Químico farmacéutico'},
		{ value: 'Marisol Jimenez', name: 'Marisol Jimenez', type: 'Hepatitis'},
	];

	profesionalesFiltro: Array<Object> = [];

	pacientes: Array<Object> = [
		{ value: 'Alberto Gonzales', name: 'Alberto Gonzales', cc: '5887458' },
		{ value: 'Jaime Perez', name: 'Jaime Perez', cc: '98785456' },
		{ value: 'Roberto Gutierrez', name: 'Roberto Gutierrez', cc: '23659854' },
		{ value: 'Natalia Alvarez', name: 'Natalia Alvarez', cc: '54857956' },
		{ value: 'Catalina Tabares', name: 'Catalina Tabares', cc: '212254854' },
		{ value: 'Elkin Jaramillo', name: 'Elkin Jaramillo', cc: '1000565545' },
	];

	/** Contiene toda la información de la cita */
	data: Object = {
		paciente: '',
		documento: '',
		especialidad: '',
		profesional: ''
	};

	constructor(
		private _eventsService: EventsService,
		private _resolver: ComponentFactoryResolver
	) {};

	/**
	 * Metodo para asignar al modelo el numero de documento del paciente.
	 * @param name {String}
	 */
	setDocument(name: string) {
		if (name != "") {
			let pacient = this.pacientes.find((pacient, idx, obj) => pacient['name'] === name);
			this.data['documento'] = pacient['cc'];
		} else {
			this.data['documento'] = '';
		}	
	}

	/**
	 * Metodo para cargar los profesionales según la especialidad solicitada.
	 * @param especialidad {String}
	 */
	setProfessional(especialidad: string) {
		this.profesionalesFiltro = this.profesionales.filter((proffessional) => proffessional['type'] === especialidad);
	}

	/**
	 * Metodo para validar que el campo documento y el profesional no esten vacios.
	 */
	validateData() {
		if (this.data['documento'] != '' && this.data['profesional'] != '' ) {
			let agenda = <Array<Object>> this.getAgenda(this.data['profesional']);
			this.calendarContainer.clear();
			this.setCalendar(agenda);
		}
	}

	/**
	 * Metodo para obtener la agenda de un profesional, se consulta por su nombre.
	 * @param profesional {String}
	 */
	getAgenda(profesional: string) {
		let agenda: Object = {};
		agenda = this._eventsService.getAgenda(profesional);
		return agenda;
	}

	/**
	 * Metodo para poblar el calendario con la agenda del profesional.
	 * Nota importante: CalendarComponent no es totalmente un componente Angular2, debido a esto las propiedades @Input
	 * y @Output no son bindeadas y se debe re-generar el componente cada vez que se actualice la agenda.
	 * @param agenda {Array}
	 */
	setCalendar(agenda: Array<Object>) {
		this.options['events'] = agenda;
		let factory = this._resolver.resolveComponentFactory(CalendarComponent);
		let component = this.calendarContainer.createComponent(factory);
		component.instance.options = this.options;
	}

	/**
	 * Metodo para controlar la asignación de citas, valida su disponibilidad y confirma su asignación
	 * @param eventClicked {Object}
	 */
	manageEventClick(eventClicked: Object) {
		if (eventClicked['backgroundColor'] === 'red') {
			toast('Esta cita ya se encuentra asignada, por favor seleccione otra', 3000);
		} else {
			let datetime = eventClicked['start'].format();
			let [date, time] = datetime.split('T');
			let [year, month, day] = date.split('-');
			let [hours, minutes, seconds] = time.split(':');
			let conf = confirm(`
								Estas seguro de asignar esta cita para el dia: ${day} del mes: ${month} 
								a las ${hours}:${minutes} con el profesional: ${this.data['profesional']}`);
			if (conf) this.assign(eventClicked, this.data['profesional']);
			return;
		}
	}

	/**
	 * Metodo para asignar una cita con un profesional a una persona, se consume el servicio.
	 * @param event {Object}
	 * @param professional {String}
	 */
	assign(event: Object, professional: string) {
		let newAgenda = this._eventsService.assignEvent(event, professional);
		let calendar = this.calendarContainer.clear();
		let factory = this._resolver.resolveComponentFactory(CalendarComponent);
		let component = this.calendarContainer.createComponent(factory);
		component.instance.options = this.options;
	}

}





