import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { CalendarComponent } from 'ap-angular2-fullcalendar';
import { MaterializeModule } from 'angular2-materialize';
import { AppComponent } from './app.component';
import { EventsService } from './shared/services/events.service';

describe('CalendarComponentTest', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        CalendarComponent
      ],
      imports: [
        HttpModule,
        FormsModule,
        MaterializeModule
      ],
      providers: [EventsService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture  = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it(`CalendarComponentTest - should set the pacient document`, () => {
    component.pacientes = [{ value: 'Alberto Gonzales', name: 'Alberto Gonzales', cc: '5887458' }];
    component.setDocument("Alberto Gonzales");
    expect(component.data['documento']).toEqual('5887458');
  });

  it('CalendarComponentTest - should set empty document', () => {
    component.setDocument('');
    expect(component.data['documento']).toEqual('');
  });

  it('CalendarComponentTest - should set the list of professionals based on one speciality', () => {
    component.profesionales = [
      { value: 'Guillermo Calderon', name: 'Guillermo Calderon', type: 'Reumatología' }, 
		  { value: 'Maria Camila Rendon', name: 'Maria Camila Rendon', type: 'Reumatología' },
    ];
    component.setProfessional('Reumatología');
    expect(component.profesionalesFiltro.length).toBeGreaterThan(0);
    expect(component.profesionalesFiltro.length).toBeLessThan(3);
  });
});
