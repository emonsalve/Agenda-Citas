import { Injectable } from '@angular/core';


@Injectable()
export class EventsService {

	/**
	 * Simulación de datos (Agendas de los profesionales)
	 */
	agendas: any = {
		'Guillermo Calderon': [
			{id: 1, allDay: false, title: 'Disponible', start: '2017-04-11T12:00:00', end: '2017-04-11T14:00:00', backgroundColor: 'gray' },
			{id: 2, allDay: false, title: 'Disponible', start: '2017-04-11T14:00:00', end: '2017-04-11T16:00:00', backgroundColor: 'gray' },
			{id: 3, allDay: false, title: 'Disponible', start: '2017-04-11T16:00:00', end: '2017-04-11T18:00:00',backgroundColor: 'gray' },
			{id: 4, allDay: false, title: 'Disponible', start: '2017-04-11T18:00:00', end: '2017-04-11T20:00:00',backgroundColor: 'gray' }
		],
		'Maria Camila Rendon': [
			{id: 1, allDay: false, title: 'Disponible', start: '2017-04-11T06:00:00', end: '2017-04-11T08:00:00', backgroundColor: 'gray' },
			{id: 2, allDay: false, title: 'Disponible', start: '2017-04-11T08:00:00', end: '2017-04-11T10:00:00', backgroundColor: 'gray' },
			{id: 3, allDay: false, title: 'Disponible', start: '2017-04-11T10:00:00', end: '2017-04-11T12:00:00',backgroundColor: 'gray' },
			{id: 4, allDay: false, title: 'Disponible', start: '2017-04-11T14:00:00', end: '2017-04-11T16:00:00',backgroundColor: 'gray' },
			{id: 5, allDay: false, title: 'Disponible', start: '2017-04-11T16:00:00', end: '2017-04-11T18:00:00',backgroundColor: 'gray' },
			{id: 6, allDay: false, title: 'Disponible', start: '2017-04-11T18:00:00', end: '2017-04-11T20:00:00',backgroundColor: 'gray' }
		],
		'German Cano Alvarez': [
			{id: 1, allDay: false, title: 'Asignada', start: '2017-04-11T18:00:00', end: '2017-04-11T20:00:00',backgroundColor: 'red' },
			{id: 2, allDay: false, title: 'Disponible', start: '2017-04-11T20:00:00', end: '2017-04-11T22:00:00',backgroundColor: 'gray' },
			{id: 3, allDay: false, title: 'Disponible', start: '2017-04-11T22:00:00', end: '2017-04-11T24:00:00',backgroundColor: 'gray' },
			{id: 4, allDay: false, title: 'Disponible', start: '2017-04-11T12:00:00', end: '2017-04-11T02:00:00',backgroundColor: 'gray' },
			{id: 5, allDay: false, title: 'Disponible', start: '2017-04-11T04:00:00', end: '2017-04-11T06:00:00',backgroundColor: 'gray' },
			{id: 6, allDay: false, title: 'Disponible', start: '2017-04-11T08:00:00', end: '2017-04-11T10:00:00',backgroundColor: 'gray' }
		],
		'Gloria Quintero': [
			{id: 1, allDay: false, title: 'Asignada', start: '2017-04-11T06:00:00', end: '2017-04-11T08:00:00',backgroundColor: 'red' },
			{id: 2, allDay: false, title: 'Disponible', start: '2017-04-11T08:00:00', end: '2017-04-11T10:00:00',backgroundColor: 'gray' },
			{id: 3, allDay: false, title: 'Disponible', start: '2017-04-11T10:00:00', end: '2017-04-11T12:00:00',backgroundColor: 'gray' },
			{id: 4, allDay: false, title: 'Asignada', start: '2017-04-11T14:00:00', end: '2017-04-11T16:00:00',backgroundColor: 'red' }
		],
		'Maria Fernanda Restrepo': [
			{id: 1, allDay: false, title: 'Asignada', start: '2017-04-11T06:00:00', end: '2017-04-11T08:00:00',backgroundColor: 'red' },
			{id: 2, allDay: false, title: 'Disponible', start: '2017-04-11T08:00:00', end: '2017-04-11T10:00:00',backgroundColor: 'gray' },
			{id: 3, allDay: false, title: 'Disponible', start: '2017-04-11T10:00:00', end: '2017-04-11T12:00:00',backgroundColor: 'gray' },
			{id: 4, allDay: false, title: 'Disponible', start: '2017-04-11T14:00:00', end: '2017-04-11T16:00:00',backgroundColor: 'gray' },
			{id: 5, allDay: false, title: 'Disponible', start: '2017-04-11T16:00:00', end: '2017-04-11T18:00:00',backgroundColor: 'gray' },
			{id: 6, allDay: false, title: 'Disponible', start: '2017-04-11T18:00:00', end: '2017-04-11T20:00:00',backgroundColor: 'gray' }
		],
		'Tatiana Quintana': [
			{id: 1, allDay: false, title: 'Disponible', start: '2017-04-11T06:00:00', end: '2017-04-11T08:00:00', backgroundColor: 'gray' },
			{id: 2, allDay: false, title: 'Disponible', start: '2017-04-11T08:00:00', end: '2017-04-11T10:00:00', backgroundColor: 'gray' },
			{id: 3, allDay: false, title: 'Disponible', start: '2017-04-11T10:00:00', end: '2017-04-11T12:00:00',backgroundColor: 'gray' },
			{id: 4, allDay: false, title: 'Disponible', start: '2017-04-11T14:00:00', end: '2017-04-11T16:00:00',backgroundColor: 'gray' },
			{id: 5, allDay: false, title: 'Disponible', start: '2017-04-11T16:00:00', end: '2017-04-11T18:00:00',backgroundColor: 'gray' },
			{id: 6, allDay: false, title: 'Disponible', start: '2017-04-11T18:00:00', end: '2017-04-11T20:00:00',backgroundColor: 'gray' }
		],
		'Valentina Rendon': [
			{id: 1, allDay: false, title: 'Disponible', start: '2017-04-11T06:00:00', end: '2017-04-11T08:00:00', backgroundColor: 'gray' },
			{id: 2, allDay: false, title: 'Disponible', start: '2017-04-11T08:00:00', end: '2017-04-11T10:00:00', backgroundColor: 'gray' },
			{id: 3, allDay: false, title: 'Disponible', start: '2017-04-11T10:00:00', end: '2017-04-11T12:00:00',backgroundColor: 'gray' },
			{id: 4, allDay: false, title: 'Disponible', start: '2017-04-11T14:00:00', end: '2017-04-11T16:00:00',backgroundColor: 'gray' },
			{id: 5, allDay: false, title: 'Disponible', start: '2017-04-11T16:00:00', end: '2017-04-11T18:00:00',backgroundColor: 'gray' },
			{id: 6, allDay: false, title: 'Disponible', start: '2017-04-11T18:00:00', end: '2017-04-11T20:00:00',backgroundColor: 'gray' }
		],
		'Marisol Jimenez': [
			{id: 1, allDay: false, title: 'Disponible', start: '2017-04-11T06:00:00', end: '2017-04-11T08:00:00', backgroundColor: 'gray' },
			{id: 2, allDay: false, title: 'Disponible', start: '2017-04-11T08:00:00', end: '2017-04-11T10:00:00', backgroundColor: 'gray' },
			{id: 3, allDay: false, title: 'Disponible', start: '2017-04-11T10:00:00', end: '2017-04-11T12:00:00',backgroundColor: 'gray' },
			{id: 4, allDay: false, title: 'Disponible', start: '2017-04-11T14:00:00', end: '2017-04-11T16:00:00',backgroundColor: 'gray' },
			{id: 5, allDay: false, title: 'Disponible', start: '2017-04-11T16:00:00', end: '2017-04-11T18:00:00',backgroundColor: 'gray' },
			{id: 6, allDay: false, title: 'Disponible', start: '2017-04-11T18:00:00', end: '2017-04-11T20:00:00',backgroundColor: 'gray' }
		]
	}

	/**
	 * Metodo que retorna la agenda especifica de un profesional.
	 * @param proffessional {String}
	 */
	getAgenda(proffessional: string) {
		return this.agendas[proffessional];
	}

	/**
	 * Metodo encargado de actualizar la agenda de un profesional con la nueva cita.
	 * @param event {Object}
	 * @param professional {String}
	 */
	assignEvent(event: Object, professional: string) {
		let newAgenda = this.agendas[professional].map((ev) => {
			if (ev['id'] === event['id']) {
				ev['backgroundColor'] = 'red';
				ev['title'] = 'Asignada';
			}
			return ev;
		});
		this.agendas[professional] = newAgenda;
		return newAgenda;
	}
}