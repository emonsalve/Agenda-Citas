import { AgendaCitasPage } from './app.po';

describe('agenda-citas App', () => {
  let page: AgendaCitasPage;

  beforeEach(() => {
    page = new AgendaCitasPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
