# AgendaCitas
Este proyecto es una prueba de Angular2 generada en Personal Soft y desarrollada por Edward Monsalve.

## Instalar las dependencias e iniciar el servidor
Para ejecutar el proyecto, primero debes clonar el repositorio en tu equipo, luego es necesario instalar las dependencias ejecutando el comando `npm install` o en su forma abreviada `npm i`.
Una vez termine el proceso de instalar las dependencias, iniciar el servidor con:  `ng serve` y navegar a:
`http://localhost:4200/`.

